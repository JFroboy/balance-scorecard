<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Goal;
use App\Indicator;
use App\Role;
use App\Initiative;
use App\Objective;

class ObjectiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $objective = Objective::all();
        return $objective;
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role = Role::find($request->id_rol);
        $objs = $role->Objectives;

        if(count($objs)<10){
            $objetivo = new Objective;
            $objetivo->nombre = $request->objetivo;
            $objetivo->id_rol = $request->id_rol;
            $objetivo->save();

            $meta = new Goal;
            $meta->descripcion = $request->meta;
            $meta->id_objective = $objetivo->id;

            $indicador = new Indicator;
            $indicador->descripcion = $request->indicador;
            $indicador->id_objective = $objetivo->id;

            $iniciativa = new Initiative;
            $iniciativa->descripcion = $request->iniciativa;
            $iniciativa->id_objective = $objetivo->id;

            $meta->save();
            $indicador->save();
            $iniciativa->save();

            return response()->json
            ([
                true
            ], 200);
        }

        else{
            return response()->json
            ([
                false
            ], 200);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Objective  $objective
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);
        $objectivos = $role->Objectives;
        return $objectivos;
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Objective  $objective
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objective = Objective::find($id);
        $nombreObj = $request->input('nombre');

        $objective->nombre = $nombreObj;
        $objective->save();

        return response()->json
        ([
            "El Objetivo fue modificado correctamente."
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Objective  $objective
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objective = Objective::find($id);
        $objective->Goals()->delete();
        $objective->Initiatives()->delete();
        $objective->Indicators()->delete();
        $objective->delete();

        return response()->json 
        ([
            "El Objetivo fue eliminado correctamente"
        ], 200);

    }
}
