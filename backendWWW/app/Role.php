<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'nombre',
    ];

    public function user()
        {
            return $this->hasOne('App\user');
        }

    public function Objectives()
    	{
        	return $this->hasMany('App\Objective',
            	'id_rol');
    	}
}
