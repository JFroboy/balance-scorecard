<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Indicator extends Model
{
    protected $table = 'indicators';

    protected $fillable = [ 'descripcion','id_objective'];

    public function objective()
    {
        return $this->hasOne('App\Objective',
        'id','id_objective');
    }
}
