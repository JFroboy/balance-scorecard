import React, { Component } from 'react';

import { Button } from 'reactstrap';
import Header from '../home/header/Header.js';
import Formulario from '../home/formulario/Formulario.js';
import Team from '../home/team/Team.js';
import Cliente from '../../pages/Cliente.js';
import Financiero from '../../pages/Financiero.js';
import Procesos from '../../pages/Procesos.js';
import Aprendizaje from '../../pages/Aprendizaje.js';
import Login from '../login/Login.js';
import {FakeAuth as fakeAuth} from './Fake.js';

import{
  BrowserRouter as Router,
  Route, Link, Redirect, withRouter, Switch} from 'react-router-dom';

const AuthButton = withRouter ( ({history}) => (
    (fakeAuth.isAuthenticated)
    ?
      <div>
        <h2> Bienvenido </h2>
        <Button onClick={() => fakeAuth.signout( ()=> history.push('/'))}>
          Salir
        </Button>
      </div>
    :
      <h1>
        No estas logueado
      </h1>
))

const AuthSite = () => (
    <div>
      <br/>
      <br/>
      <Switch>
        <PrivateRoute path="/" exact component={ Header } />
        <PrivateRoute path="/home" exact component={ Header } />
        <Route path="/login" exact component={ Login } />
        <PrivateRoute path="/cliente" exact component={ Cliente } />
        <PrivateRoute path="/financiero" exact component={ Financiero } />
        <PrivateRoute path="/procesos-internos" exact component={ Procesos } />
        <PrivateRoute path="/aprendizaje-crecimiento" exact component={ Aprendizaje } />
        <PrivateRoute path="/estrategia/crear" exact component={Formulario}/>
      </Switch>
    </div>
)

const PrivateRoute = ( { component: Component, rest } ) => (
  <Route {...rest} render={(props) => (
      fakeAuth.isAuthenticated
        ? <Component {...props}/>
        : <Redirect to={{pathname: '/login', state: {from: props.location }
      }}/>
  )}
  />
)

export default AuthSite;
