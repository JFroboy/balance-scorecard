import React, { Component } from 'react';
import {Switch, Route} from 'react-router-dom';

import Header from '../home/header/Header.js';
import Formulario from '../home/formulario/Formulario.js';
import Team from '../home/team/Team.js';
import Cliente from '../../pages/Cliente.js';
import Financiero from '../../pages/Financiero.js';
import Procesos from '../../pages/Procesos.js';
import Aprendizaje from '../../pages/Aprendizaje.js';

class Routes extends Component{
  render(){
    return(
      <Switch>
        <Route path="/" exact component={ Header } />
        <Route path="/home" exact component={ Header } />
        <Route path="/estrategia/crear" exact component={ Formulario } />
        <Route path="/cliente" exact component={ Cliente } />
        <Route path="/financiero" exact component={ Financiero } />
        <Route path="/procesos-internos" exact component={ Procesos } />
        <Route path="/aprendizaje-crecimiento" exact component={ Aprendizaje } />
        {
          /*



          */
        }
      </Switch>
    )
  }
}

export default Routes;
