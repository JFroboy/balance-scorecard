import React, {Component} from 'react';

import axios from 'axios';

import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import "react-tabs/style/react-tabs.css";
import "./style.css";
import Objetivos from './objetivos/Objetivos.js';

import {Button, Badge, Modal, ModalHeader, ModalBody} from 'reactstrap';
import SweetAlert from 'sweetalert2-react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';

const nuevoElementoSchema = Yup.object().shape({
  descripcion: Yup.string()
    .min(3, 'Debe tener mas de 3 caracteres')
    .required('El campo no puede estar vacio'),
  tipo: Yup.string()
      .required('El campo no puede estar vacio'),
});

class Roles extends Component{
  constructor(props){
    super(props);
    this.area = props.area;
    this.role = props.role;

    this.state = {
      modal: false,
      sweetShow: false,
      sweetTitle: '',
      sweetText: '',
      sweetType: '',
      objetivos : [
    ]
  }
    this.eliminar = this.eliminar.bind(this);
    this.idO = 0;
    this.title = "";
    this.change = this.change.bind(this);
    this.toggle = this.toggle.bind(this);
    this.guardar = this.guardar.bind(this);

    this.nuevoElemento = {
      descripcion: "",
      id_objective: 0,
      tipo: "",
    }
  }

  toggle() {
   this.setState(prevState => ({
     modal: !prevState.modal
   }));
  }

  change(val){
    this.idO = (this.state.objetivos[val].id);
    this.title = (this.state.objetivos[val].nombre)
  }

  guardar(value){

    this.toggle();
    value.id_objective = this.idO;

    if (parseInt(value.tipo) == 1){
      URL = "http://localhost:8000/goals/"
    }
    else if (parseInt(value.tipo) == 2){
      URL = "http://localhost:8000/indicators/"
    }
    else{
      URL = "http://localhost:8000/initiatives/"
    }

    axios({
        method: 'post',
        url: URL,
        data : value
    }).then(respuesta=>{
        let datos = respuesta.data;
        if(datos.length>0){
            this.setState({
                sweetShow: true,
                sweetText: "Se ha guardado con exito",
                sweetType: "success"
            });


        }else{
            this.setState({
                sweetShow: true,
                sweetText: "Ocurrio un error al guardar",
                sweetType: "error"
            });
        }
    });
    }

  eliminar(){
    axios({
      method: 'delete',
      url: `http://localhost:8000/objectives/${parseInt(this.idO)}`,
    }).then(respuesta=>{
        let datos = respuesta.data;
        if(datos.length>0){
          this.setState({
            sweetShow: true,
            sweetTitle: 'Notificación',
            sweetText: 'La estrategia ' + this.title +' ha sido eliminada',
            sweetType: 'success',
          })
        }
        else{
          this.setState({
            sweetShow: true,
            sweetTitle: 'Notificación',
            sweetText: 'Error al eliminar la estrategia ' +  this.title,
            sweetType: 'error',
          })
        }
      })
  }

  componentDidMount(){
    axios({
      method: 'get',
      url: `http://localhost:8000/objectives/${this.role}`
    }).then(respuesta=>{
      let datos = respuesta.data;
      if(datos.length>0){
        this.setState({
            objetivos : datos
        });
      }
      else{
        console.log("Debe redirecionar al formulario");
      }
    })
  }

  render(){
    return(
      //{/*  Services  */}
    <section className="portfolio" id="section-roles">
      <div className="container">
        <div className="col-lg-12 text-center">
          <h2 className="section-heading">Área</h2>
        </div>
        <br/>
        <div className="col-lg-12 text-center">
          <div className="intro-lead-in">
            <h4> {this.area} </h4>
          </div>
        </div>
        <br/>
        <div className="col-lg-12 text-center">
            <Tabs defaultIndex={1} onSelect={index =>  this.change(index) }>
              <TabList class="tabs clearfix" data-tabgroup="first-tab-group">
                {
                  this.state.objetivos.map((objetivo, i) => {
                        return (
                            <Tab>
                              <h6 className="section-heading text-muted">{i+1}. {objetivo.nombre} </h6>
                            </Tab>
                        )
                      })
                }
              </TabList>
              <br/>
              <br/>
              {
                this.state.objetivos.map(objetivo => {
                      return (
                          <TabPanel>
                          <Objetivos
                              idObjetivo = {objetivo.id}
                            />
                          </TabPanel>
                      )
                    })
              }
            </Tabs>
          </div>
          <br/>
          <div class="tabgroup row col-sm-12">
              <div className="col-sm-10">
                <span>
                  <Button className="button" color="success" size="lg" onClick={this.toggle}>
                    <h4> Agregar Elemento </h4>
                  </Button>
                  <b/>
                  <Button color="danger" size="lg" onClick={this.eliminar}>
                    <h4> Borrar Objetivo </h4>
                  </Button>
                </span>
              </div>
          </div>
          <br/>
          <br/>
          <Modal isOpen={this.state.modal} toggle={this.toggle}>
            <ModalHeader toggle={this.toggle}> Nuevo Elemento </ModalHeader>
            <ModalBody>
              <Formik
                  initialValues={this.nuevoElemento}
                  validationSchema={nuevoElementoSchema}
                  onSubmit={value=>{
                      this.guardar(value);
                  }}
              >
                {({ errors, touched, values }) => (
                    <Form>
                      <Field component="textarea" name="descripcion" className="input1" type="text"/>
                        {errors.descripcion && touched.descripcion ? (
                          <div className="col-sm-12">
                              <Badge color="warning"> {errors.descripcion} </Badge>
                          </div>
                        ) : null}
                        <span className="shadow-input1"></span>
                        <br />

                      <Field component="select" name="tipo" className="form-control">
                          <option value="">Seleccionar</option>
                          <option value={1} key={1} > Meta </option>
                          <option value={2} key={2} > Indicador </option>
                          <option value={3} key={3} > Iniciativa </option>
                      </Field>
                      {errors.tipo && values.tipo == "" ? (
                        <div className="col-sm-12">
                            <Badge color="warning"> {errors.tipo} </Badge>
                        </div>
                      ) : null}
                      <br/>
                      <div className="col-12">
                          <button type="submit" className="btn btn-warning float-right">Guardar</button>
                        </div>
                    </Form>
              )}
              </Formik>
            </ModalBody>
          </Modal>
          <SweetAlert
            show={this.state.sweetShow}
            title= {this.state.sweetTitle}
            text= {this.state.sweetText}
            type= {this.state.sweetType}
            onConfirm={() => this.setState({ sweetShow: false })}
          />
      </div>
    </section>
    )
  }
}

export default Roles;
