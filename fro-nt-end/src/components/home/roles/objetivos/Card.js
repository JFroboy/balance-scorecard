import React, {Component} from 'react';

import { Button, Modal, ModalHeader, ModalBody} from 'reactstrap';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { Badge} from 'reactstrap';
import SweetAlert from 'sweetalert2-react';

import axios from 'axios';

import './styleCard.css';

const validacionActualizar = Yup.object().shape({
  descripcion: Yup.string()
    .min(3, 'Debe tener mas de 3 caracteres')
    .required('El campo no puede estar vacio'),
});

class Objetivos extends Component{
  constructor(props){
    super(props);
    this.state = {
      modal: false,
      sweetShow: false,
      sweetTitle: '',
      sweetText: '',
      sweetType: '',
      title: props.titulo,
      id: props.id,
      descripcion: props.descripcion,
      url: props.url
    };

    this.toggle = this.toggle.bind(this);
    this.modificar = this.modificar.bind(this);
    this.eliminar = this.eliminar.bind(this);
  }

  toggle() {
   this.setState(prevState => ({
     modal: !prevState.modal
   }));
  }

  modificar(value){
    this.toggle();
    axios({
      method: 'put',
      url: `${this.state.url}${parseInt(this.state.id)}`,
      data: value
    }).then(respuesta=>{
        let datos = respuesta.data;
        if(datos.length>0){
          this.setState({
            sweetShow: true,
            sweetTitle: 'Notificación',
            sweetText: 'La ' + this.state.title +' ha sido actualizada',
            sweetType: 'success',
            descripcion: value.descripcion
          })
        }
        else{
          this.setState({
            sweetShow: true,
            sweetTitle: 'Notificación',
            sweetText: 'Error al actualizar la ' + this.state.title,
            sweetType: 'error',
          })
        }
      })
  }

  eliminar(){
    axios({
      method: 'delete',
      url: `${this.state.url}${parseInt(this.state.id)}`,
    }).then(respuesta=>{
        let datos = respuesta.data;
        if(datos.length>0){
          this.setState({
            sweetShow: true,
            sweetTitle: 'Notificación',
            sweetText: 'La ' + this.state.title +' ha sido eliminada',
            sweetType: 'success',
          })
        }
        else{
          this.setState({
            sweetShow: true,
            sweetTitle: 'Notificación',
            sweetText: 'Error al eliminar la ' + this.title,
            sweetType: 'error',
          })
        }
      })
  }

  render(){
    return(
      <div className="card text-center  col-sm-3 margenCard">
        <div className="card-header">
          <h5 className="card-title">{this.state.title}</h5>
        </div>
        <div className="card-body">
          <h6 className="card-text"> {this.state.descripcion} </h6>
        </div>
        <div className="card-footer text-muted">
          <Button className="btn btn-primary btn-x2 glyphicon glyphicon-arrow-right"
                  onClick={this.toggle}> Editar </Button>
          <Button className="btn btn-danger btn-x2 glyphicon glyphicon-arrow-right"
              onClick={this.eliminar}> Borrar </Button>
        </div>
        <Modal isOpen={this.state.modal} toggle={this.toggle}>
          <ModalHeader toggle={this.toggle}>Actualizar {this.state.title}</ModalHeader>
          <ModalBody>
            <Formik
                initialValues={this.state}
                validationSchema={validacionActualizar}
                onSubmit={value=>{
                    this.modificar(value);
                }}
            >
              {({ errors, touched, values }) => (
                  <Form>
                    <Field component="textarea" name="descripcion" className="input1" type="text"/>
                      {errors.descripcion && touched.descripcion ? (
                        <div className="col-sm-12">
                            <Badge color="warning"> {errors.descripcion} </Badge>
                        </div>
                      ) : null}
                      <span className="shadow-input1"></span>

                      <br />
                      <div className="col-12">
                        <button type="submit" className="btn btn-warning float-right">Modificar</button>
                      </div>
                  </Form>
            )}
            </Formik>
          </ModalBody>
        </Modal>
        <SweetAlert
          show={this.state.sweetShow}
          title= {this.state.sweetTitle}
          text= {this.state.sweetText}
          type= {this.state.sweetType}
          onConfirm={() => this.setState({ sweetShow: false })}
        />
      </div>
    )
  }
}

export default Objetivos;
