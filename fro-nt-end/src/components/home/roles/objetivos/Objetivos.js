import React, {Component} from 'react';
import Card from './Card.js';
import axios from 'axios';

class Objetivos extends Component{
  constructor(props){
    super(props);
    this.idObjetivo = props.idObjetivo;
    this.state = {
      metas: [],
      iniciativas: [],
      indicadores: []
    }

    this.obtenerMetas = this.obtenerMetas.bind(this);
    this.obtenerIndicadores = this.obtenerIndicadores.bind(this);
    this.obtenerIniciativas = this.obtenerIniciativas.bind(this);
  }

  componentDidMount(){
    this.obtenerMetas();
    this.obtenerIndicadores();
    this.obtenerIniciativas();
  }

  obtenerMetas(){
    axios({
      method: 'get',
      url: `http://localhost:8000/goals/${parseInt(this.idObjetivo)}`,
    }).then(respuesta=>{
      let datos = respuesta.data;
      if(datos.length>0){
        this.setState({
            metas : datos
        });
      }
      else{
        console.log("Nothing");
      }
    })
  }

  obtenerIndicadores(){
    axios({
      method: 'get',
      url: `http://localhost:8000/indicators/${parseInt(this.idObjetivo)}`,
    }).then(respuesta=>{
      let datos = respuesta.data;
      if(datos.length>0){
        this.setState({
            indicadores : datos
        });
      }
      else{
        console.log("Nothing");
      }
    })
  }

  obtenerIniciativas(){
    axios({
      method: 'get',
      url: `http://localhost:8000/initiatives/${parseInt(this.idObjetivo)}`,
    }).then(respuesta=>{
      let datos = respuesta.data;
      if(datos.length>0){
        this.setState({
            iniciativas : datos
        });
      }
      else{
        console.log("Nothing");
      }
    })
  }

  render(){
    return(
      <div className="margin row">
        {
          this.state.metas.map(meta=>{
            return(
              <Card
                titulo = {"Meta"}
                id = {meta.id}
                descripcion = {meta.descripcion}
                url = "http://localhost:8000/goals/"
              />
            )
          })
        }

        {
          this.state.iniciativas.map(iniciativa=>{
            return(
              <Card
                titulo = {"Iniciativa"}
                id = {iniciativa.id}
                descripcion = {iniciativa.descripcion}
                url = "http://localhost:8000/initiatives/"
              />
            )
          })
        }

        {
          this.state.indicadores.map(indicador=>{
            return(
              <Card
                titulo = {"Indicador"}
                id = {indicador.id}
                descripcion = {indicador.descripcion}
                url = "http://localhost:8000/indicators/"
              />
            )
          })
        }
      </div>
    )
  }
}

export default Objetivos;
