import React, {Component} from 'react';

import './FormularioStyle.css';

import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import {Badge} from 'reactstrap';

//import axios from 'axios';

const nuevoUsuario = Yup.object().shape({
  userName: Yup.string()
    .min(5, 'Too Short!')
    .required('Required'),
  passWord: Yup.string()
    .required('Required'),
  area: Yup.string()
    .required('Required'),
});

class FormularioUser extends Component{
  constructor(props){
    super(props);
    this.state = {
      areas : [{id: 1, name: "Cliente"}, {id: 2, name: "Financiero"}]
    }
  }

  listar(){
      if(this.state.areas.length > 0){
        return this.state.areas.map((area) =>{
          return (
            <option value="{area.id}">{area.name}</option>
          )
      })
      }
  }

  render(){
    return(
      <div className="wrapper">
        <div className="form-wrapper">
        <Formik
          initialValues={{
            userName: '',
            passWord: '',
          }}
          validationSchema={nuevoUsuario}
            onSubmit={values => {
                console.log(values);
              }}>

          {({ errors, touched }) => (
            <Form className="form">
              <div className="userName">
              <label htmlFor="userName"> Username </label>
                <Field name="userName" type="text" placeholder="AabbCC"/>
                    {errors.userName && touched.userName ? (
                      <div className="col-sm-12" style={ {alignItems:'center', justifyContent:'center'} }>
                        <Badge color="warning">  {errors.userName}</Badge>
                      </div>
                    ) : null}
              </div>

              <div className="passWord">
                <label htmlFor="passWord"> Password </label>
                <Field name="passWord" type="password" placeholder="******"/>
                  {errors.passWord && touched.passWord ? (
                    <div className="col-sm-12">
                      <Badge color="warning">  {errors.passWord}</Badge>
                    </div>
                  ) : null}
              </div>

              <div className="dependencia">
                <label htmlFor="dependencia"> Dependencia </label>
                <Field name="color" component="select">
                  <option value=""> Selecionar </option>
                  {this.listar()}
                </Field>
             </div>

             <div className="createAccount">
               <button className="contact1-form-btn" type="submit">
                 <span>
                   Guardar
                 </span>
               </button>
             </div>
            </Form>
          )
        }
        </Formik>
        </div>
      </div>
    )
  }
}
export default FormularioUser;
