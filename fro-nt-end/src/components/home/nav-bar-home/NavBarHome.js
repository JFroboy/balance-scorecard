import React, { Component } from 'react';
import $ from 'jquery';
import Modal from 'react-responsive-modal';
import FormularioUser from './formularioUser/FormularioUser.js';


class NavBarHome extends Component{
    state = {
      open: false,
    };

    onOpenModal = () => {
      this.setState({ open: true });
    };

    onCloseModal = () => {
      this.setState({ open: false });
    };

    componentDidMount(){
      var navbarCollapse = function() {
        if ($("#mainNav").offset().top > 100) {
          $("#mainNav").addClass("navbar-shrink");
        } else {
          $("#mainNav").addClass("navbar-shrink");
        }
      };
      navbarCollapse();
      $(window).scroll(navbarCollapse);


  };
  render(){
    const { open } = this.state;
    return(
      //{/* Navegacion */}
      <div className="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
        <div className="container">
          <a className="navbar-brand js-scroll-trigger" href="/home">Inicio</a>
          <div className="collapse navbar-collapse" id="navbarResponsive">
            <ul className="navbar-nav text-uppercase ml-auto">
            <li className="nav-item">
              <a className="nav-link js-scroll-trigger" href="/estrategia/crear">Nueva Estrategia</a>
            </li>
              <li className="nav-item">
                <a className="nav-link js-scroll-trigger" href="/cliente">Cliente</a>
              </li>
              <li className="nav-item">
                <a className="nav-link js-scroll-trigger" href="/financiero">Financiero</a>
              </li>
              <li className="nav-item">
                <a className="nav-link js-scroll-trigger" href="/aprendizaje-crecimiento">I + D</a>
              </li>
              <li className="nav-item">
                <a className="nav-link js-scroll-trigger" href="/procesos-internos">Recursos Humanos</a>
              </li>
              <li className="nav-item">
              {
                /*
                <a className="nav-link js-scroll-trigger" onClick={this.onOpenModal}>
                  <i className="fas fa-user"></i>
                </a>
                */
              }
              </li>
            </ul>
          </div>
          <Modal width="40%" open={open} onClose={this.onCloseModal}>
            <div className="modal-dialog" role="document">
              <div className="modal-content" style={{alignItems:'center'}}>
                <div className="modal-header">
                  <h5 className="modal-title"> Nuevo usuario </h5>
                </div>
                <div className="modal-body">
                  <FormularioUser/>
                </div>
              </div>
            </div>
          </Modal>
        </div>

      </div>
    );
  }
}

export default NavBarHome;
