import React, { Component } from 'react';
import NavBarHome from '../nav-bar-home/NavBarHome.js';

class Header extends Component{
  render(){
    return(
    //{/* Header */}
    <div id='page-top'>
      <NavBarHome/>
      <header className="masthead">
        <div className="container">
          <div className="intro-text">
            <div className="intro-lead-in">Welcome To D'ROCHA S.A!</div>
            <div className="intro-heading text-uppercase">Balanced Scorecard</div>
            <a className="btn btn-primary btn-xl js-scroll-trigger glyphicon glyphicon-arrow-right" href="/estrategia/crear">Crear Estrategia</a>
          </div>
        </div>
      </header>
    </div>
    )
  }
}

export default Header;
