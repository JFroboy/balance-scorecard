import React, { Component } from "react";

import './styleForm.css';
import Idea from '../assets/img/dmrocket-idea.png';

import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';

import SweetAlert from 'sweetalert2-react';
import {Badge} from 'reactstrap';

import axios from 'axios';
import NavBarHome from '../nav-bar-home/NavBarHome.js';


const nuevoElementoSchema = Yup.object().shape({
  objetivo: Yup.string()
    .min(4, 'Debe tener mas de 3 caracteres')
    .max(15, 'Debe tener maximo de 15 caracteres')
    .required('El campo no puede estar vacio'),
  meta: Yup.string()
    .min(4, 'Debe tener mas de 3 caracteres')
    .required('El campo no puede estar vacio'),
  indicador: Yup.string()
    .min(4, 'Debe tener mas de 3 caracteres')
    .required('El campo no puede estar vacio'),
  iniciativa: Yup.string()
    .min(4, 'Debe tener mas de 3 caracteres')
    .required('El campo no puede estar vacio'),
});

class Formulario extends Component{
  constructor(props){
    super(props);
    this.state = {
      sweetShow: false,
      sweetText: "",
      sweetType: "",
    }
    this.guardar = this.guardar.bind(this);
  }

  estrategia = {
    objetivo: "",
    meta: "",
    indicador: "",
    iniciativa: "",
    id_rol: 1
  }

  guardar(value){
        axios({
            method: 'post',
            url: `http://localhost:8000/objectives/`,
            data : value
        }).then(respuesta=>{
            let datos = respuesta.data;
            console.log("Id: ", datos);
            if(datos[0]){
                this.setState({
                    sweetShow: true,
                    sweetText: "La estrategia se ha guardado con exito",
                    sweetType: "success"
                });


            }else if (!datos[0]){
                this.setState({
                    sweetShow: true,
                    sweetText: "Se alcanzo el limite de objetivos",
                    sweetType: "info"
                });
            }
            else{
              this.setState({
                  sweetShow: true,
                  sweetText: "Ocurrio un error al guardar la estrategia",
                  sweetType: "error"
              });
            }
        });
    }

  componentDidMount(){
  }

  render(){
    return(
        <div className="container-contact1">
        <NavBarHome/>
          <div className="contact1-pic js-tilt" data-tilt>
            <img src={Idea} alt="IMG"/>
          </div>

          <Formik
            initialValues = {this.estrategia}
            validationSchema={nuevoElementoSchema}
              onSubmit={value=> {
                this.guardar(value);
              }}
            >

            {({ errors, touched }) => (
              <Form className="contact1-form validate-form">
                <span className="contact1-form-title">
                  Formulario
                </span>
                <div className="wrap-input1 validate-input" data-validate = "Objetivo requerido">
                  <Field name="objetivo" className="input1" type="text" placeholder="Objetivo"/>
                    {errors.objetivo && touched.objetivo ? (
                      <div className="col-sm-12" style={ {alignItems:'center', justifyContent:'center'} }>
                        <Badge color="warning">  {errors.objetivo}</Badge>
                      </div>
                    ) : null}

                  <span className="shadow-input1"></span>
                </div>

                <div className="wrap-input1 validate-input" data-validate = "Meta requerida">
                  <Field component="textarea" name="meta" className="input1" type="text" placeholder="Meta"/>
                    {errors.meta && touched.meta ? (
                      <div className="col-sm-12">
                        <Badge color="warning">  {errors.meta}</Badge>
                      </div>
                    ) : null}
                  <span className="shadow-input1"></span>
                </div>

                <div className="wrap-input1 validate-input" data-validate = "Indicador requerido">
                  <Field component="textarea" name="indicador" className="input1" type="text" placeholder="Indicador"/>
                    {errors.indicador && touched.indicador ? (
                      <div className="col-sm-12">
                        <Badge color="warning">  {errors.indicador}</Badge>
                      </div>
                    ) : null}
                  <span className="shadow-input1"></span>
                </div>

                <div className="wrap-input1 validate-input" data-validate = "Iniciativa requerida">
                  <Field component="textarea" name="iniciativa" className="input1" type="text" placeholder="Iniciativa"/>
                    {errors.iniciativa && touched.iniciativa ? (
                      <div className="col-sm-12">
                        <Badge color="warning">  {errors.iniciativa}</Badge>
                      </div>
                    ) : null}
                  <span className="shadow-input1"></span>
                </div>

                <div className="container-contact1-form-btn">
                  <button className="contact1-form-btn" type="submit">
                    <span>
                      Guardar
                    </span>
                  </button>
                </div>
              </Form>
              )
            }
            </Formik>
            <SweetAlert
              show={this.state.sweetShow}
              title="Notificación"
              text= {this.state.sweetText}
              type= {this.state.sweetType}
              onConfirm={() => this.setState({ sweetShow: false })}
            />
        </div>
    )
  }
}

export default  Formulario;
