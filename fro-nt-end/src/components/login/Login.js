import React, { Component } from 'react';
import './css/main.css';
import './css/util.css';

import{ Redirect} from 'react-router-dom'
import {FakeAuth as fakeAuth} from '../auth/Fake.js';


class Login extends Component{
  constructor(props){
    super(props);
    this.state = {
      redirectRoute: false
    }

    this.login = this.login.bind(this)
  }

  login(){
    fakeAuth.authenticate( () => this.setState ({redirectRoute: true}))
  }

  render(){
    const {from} = this.props.location.state || {from: { pathname: "/"}}
    const { redirectRoute } = this.state

    if(redirectRoute){
      return(
        <Redirect to={from} />
      )
    }

    else{
      return(
        <div className="Login-Home">
          <div className="container-login100">
            <div className="wrap-login100 p-b-160 p-t-50">
              <div className="login100-form validate-form">
                <span className="login100-form-title p-b-43">
                  Account Login
                </span>

                <div className="wrap-input100 rs1 validate-input" data-validate = "Username is required">
                  <input className="input100" type="text" name="username"/>
                  <span className="label-input100">Username</span>
                </div>
                <div className="wrap-input100 rs2 validate-input" data-validate="Password is required">
                  <input className="input100" type="password" name="pass"/>
                  <span className="label-input100">Password</span>
                </div>

                <div className="container-login100-form-btn">
                  <button className="login100-form-btn" onClick={this.login}>
                    Sign in
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      )
    }
  }
}

export default Login;
