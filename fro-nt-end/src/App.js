import React, { Component } from 'react';

import './App.css';
import './components/home/assets/css/agency.css';

import AuthSite from './components/auth/Auth.js';

import { BrowserRouter as Router } from 'react-router-dom';

class App extends Component {
render(){
  return(
    <Router>
      <div className='App'>
        <div>
          <AuthSite/>
        </div>
      </div>
    </Router>
  );
}
}

export default App;
