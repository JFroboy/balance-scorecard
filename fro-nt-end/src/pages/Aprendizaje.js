import React, {Component} from 'react';

import Roles from '../components/home/roles/Roles.js';
import NavBarHome from '../components/home/nav-bar-home/NavBarHome.js';

class Aprendizaje extends Component{
  render(){
    return(
      <div>
        <NavBarHome/>
        <Roles
          area = "Aprendizaje"
          role = {3}
        />
      </div>
    )
  }
}

export default Aprendizaje;
