import React, {Component} from 'react';

import Roles from '../components/home/roles/Roles.js';
import NavBarHome from '../components/home/nav-bar-home/NavBarHome.js';

class Procesos extends Component{
  render(){
    return(
      <div>
        <NavBarHome/>
        <Roles
          area = "Procesos"
          role = {4}
        />
      </div>
    )
  }
}

export default Procesos;
