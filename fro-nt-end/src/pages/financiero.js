import React, {Component} from 'react';

import Roles from '../components/home/roles/Roles.js';
import NavBarHome from '../components/home/nav-bar-home/NavBarHome.js';

class Financiero extends Component{
  render(){
    return(
      <div>
        <NavBarHome/>
        <Roles
          area = "Financiero"
          role = {2}
        />
      </div>
    )
  }
}

export default Financiero;
