import React, {Component} from 'react';

import Roles from '../components/home/roles/Roles.js';
import NavBarHome from '../components/home/nav-bar-home/NavBarHome.js';

class Cliente extends Component{
  render(){
    return(
      <div>
        <NavBarHome/>
        <Roles
          area = "Cliente"
          role = {1}
        />
      </div>

    )
  }
}

export default Cliente;
